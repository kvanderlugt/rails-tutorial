# == Schema Information
#
# Table name: users
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  email      :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'spec_helper'

describe User do
  before do
  	@user = User.new( name: "TestUser", email: "test@tests.com",
                      password: "foobar", password_confirmation: "foobar" ) 
  end
  subject { @user }

  it { should respond_to(:name) }
  it { should respond_to(:email) }
  it { should respond_to(:password_digest) }
  it { should respond_to(:password) }
  it { should respond_to :password_confirmation }
  it { should respond_to :authenticate }
  it { should respond_to :remember_token }

  it { should be_valid }

  it { should respond_to :admin }
  it { should respond_to :authenticate }
  it { should_not be_admin }

  it { should respond_to :microposts }

  describe "with admin attributes set to 'true'" do
    before do
      @user.save!
      @user.toggle! :admin
    end

    it { should be_admin }
  end

  describe "user name must be present" do
  	before { @user.name = "" }
  	it { should_not be_valid }
  end

  describe "when name is too long" do
  	before { @user.name = 'a'*51 }
  	it { should_not be_valid }
  end

  describe "when email is not present" do
  	before { @user.email = "" }
  	it { should_not be_valid }
  end

  describe "when email format is invalid" do
  	it "should be valid" do
  		addresses = %w[user@foo,com user_at_foo.org example.user@foo.
                     foo@bar_baz.com foo@bar+baz.com]
      addresses.each do | bad_address |
      	@user.email = bad_address
      	@user.should_not be_valid
      end
    end
  end

  describe "when email format is valid" do
  	it "should be valid" do
  		addresses = %w[user@foo.COM A_US-ER@f.b.org frst.lst@foo.jp 
  									a+b@baz.cn]
  		addresses.each do | good_address |
  			@user.email = good_address
  			@user.should be_valid
  		end
  	end
  end

  describe "when email is not unique" do
  	before do 
  		user_with_same_email = @user.dup 
  		user_with_same_email.email = @user.email.upcase
  		user_with_same_email.save
  	end
  	it { should_not be_valid }
  end

  describe "when email is multicase" do
    let(:mixed_case_email) { "FooBar@fOBAR.Jp"}
    it "should be stored as lower case" do
      @user.email = mixed_case_email
      @user.save
      @user.reload.email.should == mixed_case_email.downcase
    end
  end

  describe "when password is not present" do
    before { @user.password = @user.password_confirmation = "" }
    it { should_not be_valid }
  end

  describe "when password does not meet confirmation" do
    before { @user.password_confirmation = "nomatch" }
    it { should_not be_valid }
  end

  describe "when password_confirmation is nil" do
    before {@user.password_confirmation = nil}
    it { should_not be_valid }
  end

  describe "when a password is too short" do 
    before { @user.password = @user.password_confirmation = 'a'*5 }
    it { should_not be_valid }
  end

  describe "return value of authenticate method" do
    before { @user.save }
    let(:found_user) { User.find_by_email(@user.email )}

    # This may not work, playing around
    describe "with valid password" do
      #it { should be found_user.authenticate(@user.password)}
      it { found_user.authenticate(@user.password).should be_true }
    end

    describe "with invalid password" do
      let(:user_for_invalid_password) { found_user.authenticate('Invalid')}
      it { should_not == user_for_invalid_password}
      specify { user_for_invalid_password.should be_false }
    end
  end

  describe "accessible attributes" do
    it "should not allow access to admin" do
      expect do
        User.new(admin: true)
      end.to raise_error(ActiveModel::MassAssignmentSecurity::Error)
    end
  end

  describe "remember token" do
    before { @user.save }
    its(:remember_token) { should_not be_blank }
  end

  describe "micropost association" do
    before { @user.save }
    let!(:older_post) { FactoryGirl.create(:micropost, user: @user, created_at: 1.day.ago )}
    let!(:newer_post) { FactoryGirl.create(:micropost, user: @user, created_at: 1.hour.ago )}

    it "should order microposts with the most recent first" do
      @user.microposts.should == [newer_post, older_post]
    end

    it "should destroy associated microposts" do
      microposts = @user.microposts.dup
      @user.destroy
      microposts.should_not be_empty
      microposts.each do |micropost|
        Micropost.find_by_id(micropost.id).should be_nil
      end
    end

    describe "status" do
      let(:unfollowed_post) { FactoryGirl.create(:micropost, user: FactoryGirl.create(:user))}
      its(:feed) { should include(newer_post)}
      its(:feed) { should include(older_post)}
      its(:feed) { should_not include(unfollowed_post)}
    end
  end
end
