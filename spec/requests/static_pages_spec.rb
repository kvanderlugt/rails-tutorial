require 'spec_helper'

describe "StaticPages" do
  subject { page }

  shared_examples_for "all static pages" do
    it { page.status_code.should be(200) }
    it {should have_selector('h1', :text => heading)}
    it {should have_selector('title', :text => full_title(page_title))}
  end

  describe "Home page" do
    before { visit root_path }
    let(:heading) { "Sample App" }
    let(:page_title) { "" }

    it_should_behave_like "all static pages"
    it {should_not have_selector('title', :text => "Home")}

    it "should have the correct links on the layout" do
      click_link "About"
      page.should have_selector 'title', :text => full_title('About Us')
      click_link "Help"
      page.should have_selector('title', :text => full_title("Help"))
      click_link "Contact"
      page.should have_selector('title', :text => full_title("Contact"))
      click_link "Home"
      click_link "Sign up!"
      page.should have_selector 'title', :text => full_title('Sign Up')
      click_link "sample app"
      page.should have_selector 'title', :text => full_title('')
    end     

    describe "viewing other users microposts" do
      let(:user)  { FactoryGirl.create(:user) }
      let(:other_user)  { FactoryGirl.create(:user) }
      before do
        FactoryGirl.create(:micropost, user: other_user, content: "Lorem")
        sign_in user
        visit root_path
      end

      it "should not show delete for others posts" do
        page.should_not have_link("delete")
      end
    end

    describe "for signed-in users" do
      let(:user)  { FactoryGirl.create(:user) }
      before do
        FactoryGirl.create(:micropost, user: user, content: "Lorem")
        FactoryGirl.create(:micropost, user: user, content: "Ipsum")
        sign_in user
        visit root_path
      end

      it "should show the users feed" do
        user.feed.each do |item|
          page.should have_selector("li##{item.id}", text: item.content)
        end
      end

      it "micropost count" do
        page.should have_content "2 microposts"
      end

      describe "pagination" do
        before (:all) { 30.times { FactoryGirl.create(:micropost, user: user, content: "A") } }
        after (:all) { Micropost.delete_all }

        it { should have_selector ('div.pagination') }

        it "should list each micropost" do
          Micropost.paginate(page: 1).each do |post|
            page.should have_selector('li', text: post.content)
          end
        end
      end
    end
  end

  describe "Help Page Tests" do
    before { visit help_path }
    let(:heading) { "Help" }
    let(:page_title) { "Help" }

    it_should_behave_like "all static pages"
  end

  describe "About page" do
    before { visit about_path }
    let(:heading) { "About" }
    let(:page_title) { "About Us" }

    it_should_behave_like "all static pages"
  end

  describe "Contact page" do
    before { visit contact_path }
    let(:heading) { "Contact" }
    let(:page_title) { "Contact" }

    it_should_behave_like "all static pages"
  end
end
