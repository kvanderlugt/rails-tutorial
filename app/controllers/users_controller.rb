class UsersController < ApplicationController
  before_filter :signed_in_user,  only:[:index, :edit, :update]
  before_filter :correct_user,    only:[:edit, :update]
  before_filter :admin_user,      only: :destroy
  before_filter :already_signed_in_user, only:[:create, :new]

  def new
  	@user = User.new
  end
  
  def show
  	@user = User.find(params[:id])
    @microposts = @user.microposts.paginate(page: params[:page])
  end

  def create
  	@user = User.new(params[:user])
  	if @user.save
      sign_in @user
  		flash[:success] = "Welcome, thanks for joining!"
  		redirect_to @user
  	else
  		render 'new'
  	end
  end

  def edit
  end

  def update
    if @user.update_attributes(params[:user])
      flash[:success] = "Profile saved"
      sign_in @user
      redirect_to @user
    else
      render 'edit'
    end
  end

  def index
    @users = User.paginate(page: params[:page])
  end

  def destroy
    user = User.find(params[:id])
    if current_user?(user)
      flash[:error] = "Don't remove yourself"
      redirect_to (root_path)
    else
      user.destroy
      flash[:success] = "User removed"
      redirect_to users_url
    end
  end

  private

    def already_signed_in_user
      if signed_in?
        flash[:notice] = "Already signed in, please sign out first"
        redirect_to root_path
      end
    end

    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_path) unless current_user?(@user)
    end

    def admin_user
      redirect_to(root_path) unless current_user.admin?
    end
end
